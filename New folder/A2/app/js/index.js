mapboxgl.accessToken = 'pk.eyJ1Ijoiam9uYXRoYW5haiIsImEiOiJja253dTVtY3cwZ3NwMnVxZHNmODR0a3VrIn0.1mxDpk5Sxb_6RidEsmLTqw';

//global variables
let map;
let tripName = "";
let country = "";
let startDate = "";
let totalDist = 0;
let markers = [];
let countryLoc = [];

//global trip variable (this will be put to the global tripList object once the user confirms his trip)
let currentTrip;



//inserts country names to drop down list
let countriesDropDownElement = document.getElementById("countries")
let countryOptionsString = "<option value=\"\"></option>"
for (let i = 0; i < countryData.length; i++){
    countryOptionsString += "<option value=\"" + countryData[i] + "\">" + countryData[i] + "</option>" 
}

countriesDropDownElement.innerHTML = countryOptionsString

//function to make a new trip
/*
This function is used to get the user's input, and make a trip object out of that. It takes the trip name, country, and the date of the trip.
It also checks if the user has made any mistakes when choosing, for example the user must fill in all three of the elements, and they can't
set a date before today. If the user has enetered everything correctly, the webpage will display a map based on the country, the table, and the undo and finish buttons.

*/

function confirmAddTrip(){
    tripName = document.getElementById("name").value
    country = document.getElementById("countries").value
    date = document.getElementById("startDate").value
    
    let newDate = new Date(date)
    let today = new Date()
	
    //These if statements just check if the user entered everything correctly.
    if(tripName === "" || country === "" || date === ""){
        alert("Please fill in either the trip name, the country, or the start date of your trip")
    }
    
    else if((newDate.getFullYear() < today.getFullYear()) || (newDate.getFullYear() === today.getFullYear() && newDate.getMonth() < today.getMonth()) || (newDate.getFullYear() === today.getFullYear() && newDate.getMonth() === today.getMonth() && newDate.getDate() < today.getDate())){
        
        alert("Please create a future date.")
    }
    else{
        if(confirm("Confirm the selected country and date?")){
            
            document.getElementById("confirmTrip").disabled = true
            document.getElementById("name").disabled = true
            document.getElementById("countries").disabled = true
            document.getElementById("startDate").disabled = true
            
            currentTrip = new Trip(tripName, date, country);
            
            
            let queryString = "https://eng1003.monash/OpenFlights/airports/?country="+ country + "&callback=makeMap"
            let script = document.createElement('script');
            script.src = queryString;
            document.body.appendChild(script);
            
            let tableStr =  "<table class=\"mdl-data-table mdl-js-data-table mdl-data-table\" style=\"width:75%\"><thead><th class=\"mdl-data-table__cell--non-numeric\">Trip</th><th>Distance(km)</th></thead><tbody align-content: center><tr><td class=\"mdl-data-table__cell--non-numeric\">Total Distance</td><td>"+ totalDist +"</td></tr></tbody></table>";
            
            //makes the html element of the update button and dropdown button in main.html
            let airportButtonsStr = ""
            airportButtonsStr +="<span class = \"inlineButtons\" >"
            airportButtonsStr += "<label for=\"airports\" style = \"font-style:20px\"> Pick your airport:</label>"
            airportButtonsStr +="<select id=\"airports\" style = \"font-style:20px\" >"
            airportButtonsStr += "</select> </span> &nbsp&nbsp&nbsp&nbsp&nbsp"
            airportButtonsStr += "<span class = \"inlineButtons\" >"
            airportButtonsStr += "<button class=\"mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent\" id = \"updateButton\" onclick = \"updateRoute()\" >"
            airportButtonsStr += "Update Your Trip"
            airportButtonsStr += "</button> </span>"
                
            document.getElementById("airportButtons").innerHTML = airportButtonsStr 
            
            
               
               //mock table addition
            document.getElementById("table").innerHTML = tableStr;
           
	
            document.getElementById("extraButtons").innerHTML = "<div id = \'undo\' onclick=\"undoAirport()\"><button class=\"mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent\" align-content: center>Undo</button></div><br><br><br><div id = \'finish\' onclick=\"saveTrip()\"><button class=\"mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent\" align-content: center>Finish</button></div>"
  
            
        }
        else{
            //nothing here
        }
        //test
    }
    
}




//This function makes the map and displays it on the webpage. It also sets the sources and layers for the lines that we will use later.
//The center of the map will be based on the country's first airport location, and we made the zoom based on australia's size.

function makeMap(results){
       
		countryLoc.push(results[0].longitude) 
		countryLoc.push(results[0].latitude)
		
        map = new mapboxgl.Map({
        container: 'map', // container id
        style: 'mapbox://styles/mapbox/streets-v11', // style URL

        center: [results[0].longitude,results[0].latitude], // starting position [lng, lat]
        zoom: 3 // starting zoom
        });
    
    //adds the sources to the map to make the lines 
   
    map.on('load', function() {
		map.addSource('route', {
			'type': 'geojson',
			'data': {
				'type': 'Feature',
				'properties': {},
				'geometry': {
					'type': 'LineString',
					'coordinates': []
				}
			}
		});
        
        map.addSource('connectingRoute', {
			'type': 'geojson',
			'data': {
				'type': 'Feature',
				'properties': {},
				'geometry': {
					'type': 'LineString',
					'coordinates': []
				}
			}
		});
        
        //adds the layers to the map
        map.addLayer({
			'id': 'route',
			'type': 'line',
			'source': 'route',
			'layout': {
				'line-join': 'round',
				'line-cap': 'round'
			},
            'paint':{
                'line-color' : '#228B22',
                'line-width' : 4
            }
            
		});
        
        map.addLayer({
			'id': 'connectingRoute',
			'type': 'line',
			'source': 'connectingRoute',
			'layout': {
				'line-join': 'round',
				'line-cap': 'round'
			},
            'paint':{
                'line-color' : '#FFD700',
                'line-width' : 2
            }
            
		});
        
        
	
		
	});
    
    //calls the getAllLocations Id function
    queryString = "https://eng1003.monash/OpenFlights/airports/?country="+ country + "&callback=getAllLocationsId"
    script = document.createElement('script');
    script.src = queryString;
    document.body.appendChild(script);
    
}

let allLocationsId = []
let airportsOnlyId = []
//The next two functions is used to filter the data which isn't an airport (does not have a route connected to it).

//This function gets the IDs of all the elements in the airports API and puts it in the allLocationsId array.
//The reason why we need to do this will be relevant in the next function, which is to filter the elements based on
//whether thay have a route from it or not.
function getAllLocationsId(results2){
    for (let i = 0; i < results2.length; i++){
        allLocationsId.push(results2[i].airportId)
    }
    
    queryString = "https://eng1003.monash/OpenFlights/allroutes/?country="+ country + "&callback=filterOnlyAirports"
    script = document.createElement('script');
    script.src = queryString;
    document.body.appendChild(script);
    
}

/*
This function filters the airports that can send out and receive flights based on their ids and pushes it the airportsOnlyId array. Since the allroute API to get the routes of
a country require the IDs, we have to use the allLocationsId.  
*/
function filterOnlyAirports(results3){
    
    for (let i = 0; i < allLocationsId.length; i++){
        for (let j = 0; j < results3.length; j++){
            
            if((allLocationsId[i] == results3[j].sourceAirportId) && !(airportsOnlyId.includes(results3[j].sourceAirportId))){
                airportsOnlyId.push(results3[j].sourceAirportId)
            }
            
            if((allLocationsId[i] == results3[j].destinationAirportId) && !(airportsOnlyId.includes(results3[j].destinationAirportId))){
                airportsOnlyId.push(results3[j].destinationAirportId)
            }
        }
    }
    
    queryString = "https://eng1003.monash/OpenFlights/airports/?country="+ country + "&callback=displayMarkersAndFillDropdown"
    script = document.createElement('script');
    script.src = queryString;
    document.body.appendChild(script);    
    
    
}

/*
This function displays the markers on the map based on the coordinates of the filtered airports (those that have a route connecting to it).
It also sets the list for the dropdown list based on the names of the filtered airports and sorts it alphabetically.
*/
function displayMarkersAndFillDropdown(results2){
    
    
    let airportsDropDownElement = document.getElementById("airports")
    let airportsOptionsString = "<option value=\"\"></option>"
    airportNames = []
    
    for (let i = 0; i <results2.length; i++){
        
        if(airportsOnlyId.includes(Number(results2[i].airportId))){
            airportNames.push(results2[i].name)
        }
    }
   
    
    
    airportNames.sort()
    
    for (let i = 0; i < airportNames.length; i++){
        airportsOptionsString += "<option value=\"" + airportNames[i] + "\">" + airportNames[i] + "</option>" 
    }

    airportsDropDownElement.innerHTML = airportsOptionsString

    
    
    for (let i = 0; i < results2.length; i++){
        if(airportsOnlyId.includes(Number(results2[i].airportId))){
            var marker = new mapboxgl.Marker({
            draggable: false,
            color : '#3FB1CE'
            })
            .setPopup(new mapboxgl.Popup().setHTML(results2[i].name))
            .setLngLat([results2[i].longitude,results2[i].latitude])
            .addTo(map);

            markers.push(marker)
                        
        }
    
    }
    
    
}
 
//The following functions adds the selected airport name to the list of airports in the global Trip object (currentTrip) and
//updates the display of the map by adding lines over the markers.
//To ensure all the data works in chronological order because of web services delay,
//We made each consecutive function to work after the first function is called.
//Kind of like a domino effect, so be careful if you want to use these functions below outside of the update route functions
///////////////////////////////START OF UPDATE ROUTE FUNCTIONS//////////////////////////////////////////
function updateRoute(){    
    queryString = "https://eng1003.monash/OpenFlights/airports/?country="+ currentTrip.country + "&callback=updateAirport"
    script = document.createElement('script');
    script.src = queryString;
    document.body.appendChild(script);

}

/*This function gets the selected airport's name, gets the relevant information from the airport(name, longlat, id) and pushes it
 to currentTrip. it also updates the table in the webpage based on what the user chooses.

*/
function updateAirport(results){
    let selectedAirportName = document.getElementById("airports").value
    let selectedAirportLonglat = []
    let selectedAirportId = ""
    if(selectedAirportName === ""){
        alert("Please select an airport")
    }
    else{
        for (let i =0; i < results.length; i++){
            if(results[i].name === selectedAirportName){
                selectedAirportLonglat = [Number(results[i].longitude),Number(results[i].latitude)]
                selectedAirportId = results[i].airportId
            }
        }
        currentTrip.addAirport(selectedAirportName, selectedAirportLonglat, selectedAirportId)
		
		
		if((currentTrip._airports.length)<2){
			
			appendRow('table',selectedAirportName,'Pending',0) 
			
		}else{
            
			for(let i = currentTrip._airports.length-1 ; i > -1; i--){
                   
            	if(currentTrip._airports[i].label === selectedAirportName){
                    
					let origAirport = currentTrip._airports[i-1].label
					deleteLastRow();
					
					let latestAirport = currentTrip.airports[currentTrip.airports.length - 1].longlat
            		let secondAirport = currentTrip.airports[currentTrip.airports.length - 2].longlat
					
					let distance = (Number(getDistance(latestAirport, secondAirport))).toFixed(2)
					
					currentTrip.addDistance(distance)
					
					
                	appendRow('table',origAirport,selectedAirportName,distance) ;
					appendRow('table',selectedAirportName,'Pending',0) ;
                    
                    break;
            	}
        	}
		}
        
       

        queryString = "https://eng1003.monash/OpenFlights/routes/?sourceAirport="+ currentTrip.airports[currentTrip.airports.length - 1].id + "&callback=getRoutesFromSelectedAirport"
        script = document.createElement('script');
        script.src = queryString;
        document.body.appendChild(script);
	}  
}

let fullDestinationAirportId = []

/*
This function gets all the destinations' IDs from the routes that are connected from that airport and pushes it to fullDestinationAirportId.
*/
function getRoutesFromSelectedAirport(results2){
    //clears the fullDestinationAirportId
    fullDestinationAirportId.splice(0, fullDestinationAirportId.length)
    
    if(results2.length === 0){
        alert ("There are no routes that are connected from this airport.")
    }
    else{
        for (let i = 0; i < results2.length; i++){
            let destAirportIdStr = ""
            destAirportIdStr += results2[i].destinationAirportId
            fullDestinationAirportId.push(destAirportIdStr)
        }
    }
    
    queryString = "https://eng1003.monash/OpenFlights/airports/?country=" + currentTrip.country + "&callback=filterDestinationAirports"
    script = document.createElement('script');
    script.src = queryString;
    document.body.appendChild(script);
    
    
    
}

let filteredDestinationAirportId = []

//this function filters out the all destination airports IDs so that only domestic flights apply in filteredDestinationAirports
function filterDestinationAirports(results3){
    filteredDestinationAirportId.splice(0, filteredDestinationAirportId.length)
    
    
    for (let i = 0; i < fullDestinationAirportId.length ; i++){
        for (let j = 0; j < results3.length; j++){
            
            if(fullDestinationAirportId[i] === results3[j].airportId){
                if (!(filteredDestinationAirportId.includes(fullDestinationAirportId[i]))){
                    filteredDestinationAirportId.push(fullDestinationAirportId[i])
                    }
                
            }
        }
    }
    
    
    
    
    repickAirports(results3)
    
    
}

let filteredDestAirportsLongLat = []
//This function sets the dropdown list based on the domestic-filtered destination airports, and also pushes the airports' coordinates to filteredDestinationAirportsLongLat
//The function also calls the displayLines function
function repickAirports(results3){
    filteredDestAirportsLongLat.splice(0, filteredDestAirportsLongLat.length)
    let airportsDropDownElement = document.getElementById("airports")
    let airportsOptionsString = "<option value=\"\"></option>"
    airportNames = []
    for (let i = 0; i <results3.length; i++){
        for (let j = 0; j < filteredDestinationAirportId.length; j++){
            if(filteredDestinationAirportId[j] === results3[i].airportId){
                 airportNames.push(results3[i].name)
                 filteredDestAirportsLongLat.push([Number(results3[i].longitude),Number(results3[i].latitude)])
            }
        }
       
    }
    
    airportNames.sort()
    
    for (let i = 0; i < airportNames.length; i++){
        airportsOptionsString += "<option value=\"" + airportNames[i] + "\">" + airportNames[i] + "</option>" 
    }

    airportsDropDownElement.innerHTML = airportsOptionsString
    
    displayLines()
    
    
}

///////////////////////////////////////END OF UPDATE ROUTE FUNCTION///////////////////////////////////////////////////////////






//displays the lines between selected airports, as well as displaying lines between the final airport and all airports that are connected to it. 
//The lines that connects the selected airports is colored green and is slightly thicker, and the lines that connects the most recent selected 
//airport to the routes that connect to it as yellow and isn't as thick as the green line.
function displayLines(){
    let tripCoordinates = []
    
    let airportList = currentTrip.airports;
    
    let coordinates=[];
    for(let i = 0;i<airportList.length;i++){
        tripCoordinates.push(airportList[i].longlat)
    }
    
    //this updates the source based on the trip object's airport list (finally did it, woohoo!)
    map.getSource('route').setData({
				'type': 'Feature',
				'properties': {},
				'geometry': {
					'type': 'LineString',
					'coordinates': tripCoordinates
                }
    })
    
    let connectedCoordinates = []
    
    for(let i = 0; i < filteredDestAirportsLongLat.length; i++){
        connectedCoordinates.push(airportList[airportList.length-1].longlat)
        connectedCoordinates.push(filteredDestAirportsLongLat[i])
    }
    
    
    map.getSource('connectingRoute').setData({
				'type': 'Feature',
				'properties': {},
				'geometry': {
					'type': 'LineString',
					'coordinates': connectedCoordinates
                }
    })
    
}
/*
function getDistance with parameter lngLat1 and lngLat 2
this function will use turf library and return the distance between two coordinaes.
*/
function getDistance(lngLat1, lngLat2){
    let options = {
        units: 'kilometers'
    };
    let distance = turf.distance(lngLat1, lngLat2, options)
    return distance.toFixed(2) 
}

/*
function appendRow with parameter tableID origOne origTwo and distUp
it should get the table ref and number of existing rows
each row has 2 columns. The first columns should be origOne to origTwo and the 2nd column should be the number of the distUp
if the exsisting row number is 1 it should append the new row to the first line after the header and display all the required info and update the total distance
other than that it should refer to the number exsiting row to update and display the required information
at the end it should update the distance number in the total distance row
*/
function appendRow(tableID,origOne,origTwo,distUp) {
  // Get a reference to the table
	let tableRef = document.getElementById(tableID);
  	let numExsRow = tableRef.tBodies[0].rows.length;

	
  if(numExsRow === 1){
    let appendRow = tableRef.insertRow(1)
    let tripCell = appendRow.insertCell(-1);
    let distCell = appendRow.insertCell(-1);
    
    let tripStr = document.createTextNode(origOne+" to "+origTwo);
    let distStr = document.createTextNode(Number(distUp));
    totalDist += Number(distUp);
   
	  
    tripCell.appendChild(tripStr);
    distCell.appendChild(distStr);
  } else {
    let appendRow = tableRef.insertRow(numExsRow);
    let tripCell = appendRow.insertCell(-1);
    let distCell = appendRow.insertCell(-1);
    
    let tripStr = document.createTextNode(origOne+" to "+origTwo);
    let distStr = document.createTextNode(Number(distUp));
    totalDist += Number(distUp);
	  
    tripCell.appendChild(tripStr);
    distCell.appendChild(distStr);
  }
  
 let totalDistCell = document.getElementById(tableID).rows[numExsRow+1].cells;
 totalDistCell[1].innerHTML = Number(totalDist).toFixed(2);

}
/*
function deleteLastRow without parameter
deleteLastRow does not necessarily mean it will delete the very last ow but the second last row, the 2nd last row is assumed to be the last row of the body where the user can see the exisiting chosen trips. it gets the reference for the table and deletes the row.
*/
function deleteLastRow() {
  // Get a reference to the table
  let tableRef = document.getElementById('table');
  let numExsRow = tableRef.tBodies[0].rows.length;
  tableRef.deleteRow(numExsRow-1);
}

/*
function undoAirport with no parameter
it should check the number of rows in the table.
if it is 1 the user will be alerted
if it is 2 it will delete the row and update the airports also allows the user to pick another starting initial airport
other than that it should get the distance of the latest trip and delete 2 last rows then update the table 
*/

function undoAirport(){
	   
	let numExsRow = document.getElementById('table').tBodies[0].rows.length;
	
	if(numExsRow <= 1){
		alert('No airports is in the list. cannot undo');
		
	}else if (numExsRow === 2) {
		deleteLastRow();
		currentTrip.popAirport();
		
		queryString = "https://eng1003.monash/OpenFlights/airports/?country="+ country + "&callback=makeMap";
        script = document.createElement('script');
        script.src = queryString;
        document.body.appendChild(script);
		
		
	}else{
		let minDist = currentTrip.distance[currentTrip.distance.length-1];
	
		
		if(typeof(minDist) === 'undefined'){
			minDist= 0 ;
		}
		
		minDist = Number(minDist).toFixed(2);
		
		deleteLastRow();
		deleteLastRow();
		currentTrip.popAirport();
		currentTrip.removeDistance();
		
		totalDist = totalDist-minDist;
		
		appendRow('table',currentTrip.airports[currentTrip.airports.length-1].label,'Pending',0); 
	}
}

/* 
function to save Trip, no parameter
it should prompt for confirmation, if the user confirms it will get all needed information and updates it to the local storage accordingly, it will then alert that it has been saved and redirects the user to the info.html. If the user does not confirm it will go back to the main page
*/
function saveTrip(){
    if(confirm("Press OK to save this trip.")){
		
		currentTrip.countryLongLat = countryLoc;
        trip.addFutureTrip(currentTrip);
    
        let index = trip.futureList.length-1;
        let data = JSON.stringify(index);
        
        updateLocalStorage(trip);
		localStorage.setItem(TRIP_INDEX_KEY, data);
		localStorage.setItem(TRIP_EVENTS_KEY, JSON.stringify('future'));
        alert("You have saved this trip!");
        window.location = "info.html"; 
    }
    else{
        window.location = "index.html";
    }
}





    




