class Trip {
	//class constructors
    constructor(newName, newDateObj, newCountry){
        this._name = newName;
        this._date = newDateObj;
		this._dateCreated = "";
        this._airports = [];
        this._distance = [];
        this._country = newCountry;
        this._countryLongLat = [];
	}
    //class getters
    get name(){
        return this._name;
    }
    get date(){
        return this._date;
    }
    get airports(){
        return this._airports;
    }
    get country(){
        return this._country;
    }
    get distance(){
        return this._distance;
    }
    get creationDate(){
        return this._dateCreated;
    }
	get countryLongLat(){
		return this._countryLongLat;
	}
	get stops(){
        
        if (this._airports.length > 2){
            return this._airports.length - 2;
        }
        else{
            return 0;
        }
    }
	//class setters
    set name(newName){
        this._name = newName;
    }
    
    set date(newDate){
        this._date = newDate;
    }
	set creationDate(newDate){
        this._dateCreated = newDate;
    }
    set country(newCountry){
        this._country = newCountry;
    }
	set countryLongLat(longLat){
		this._countryLongLat.push(longLat[0])
		this._countryLongLat.push(longLat[1])
	}
	//class methods
    addDistance(newDistance){
        this._distance.push(newDistance);
    }
	
	removeDistance(){
        this._distance.pop();
    }
    
    addAirport(newLabel, newLonglat, newId = ""){
        
        let airportObject = {
            label : newLabel,
            longlat : newLonglat,
            id : newId
        }
        this._airports.push(airportObject);
    }
    
    popAirport(){
        this._airports.pop();
    }

    fromData(data){
        this._name = data._name;
        this._date = data._date;
        this._airports = data._airports;
        this._distance = data._distance;
        this._country = data._country;
        this._countryLongLat = data._countryLongLat;
		this._dateCreated = data._dateCreated
    }
}

//Contains the trip class, divides it into future lists and past lists, and has methods such as adding it to the future list, or removing the trip. 
class TripList{
    //class constructors
    constructor(){
        this._pastList = [];
        this._futureList = [];
    }
    //class getters
    get pastList(){
        return this._pastList;
    }
    
    get futureList(){
        return this._futureList;
    }
	
	//class methods
    addFutureTrip(newTrip){
        let today = new Date().toLocaleDateString();
        newTrip.creationDate = today;
        this._futureList.push(newTrip);
    }
    
    addPastTrip(newTrip){
        this._pastList.push(newTrip);
    }
    
    
	getTrip(index,list){
		if(list === "past"){
			return this.pastList[index];
		}else{
			return this.futureList[index];
		}
		
	}
    addTrip(name, startDate, country){
        let today = new Date().toLocaleDateString();
		let Add = new Trip(name, startDate,country);
		Add.creationDate = today;
        this.futureList.push(Add);
    }
    
    removeTrip(index){
		let today = new Date();
		
		//let today = new Date().toLocaleDateString();
		
		let info = this.futureList[index];
		let theSelectedTrip = new Trip();
        theSelectedTrip.fromData(info)
		
		let tripDate = new Date(theSelectedTrip.date);
		
		if((tripDate.getFullYear() === today.getFullYear())&&(tripDate.getMonth() === today.getMonth())&&(tripDate.getDate() === today.getDate())){
			//Trip cannot be deleted
			alert("Trip cannot be deleted.");
        }
        else{
            this._futureList.splice(index,1);
        } 
	}
	
	fromData(data){
		this._pastList = data._pastList;
        this._futureList = data._futureList;
	}
}
           
//Global variables 
let singleTrip = new Trip();
let trip = new TripList();


//Key to be used for local storage
const TRIP_DATA_KEY = "tripListData";
const TRIP_INDEX_KEY = "selectedTripIndex";
const TRIP_EVENTS_KEY = "tripEventsKey";

//Function to check if there is data exist inside the local storage
function checkIfDataLocalStorageExist(){
if(typeof(localStorage.getItem(TRIP_DATA_KEY)) !== "undefined" && localStorage.getItem(TRIP_DATA_KEY) !== null && localStorage.getItem(TRIP_DATA_KEY) !== ""){
        return true;
    }
    else{
        return false;
    }
}

//Function to update the local storage
function updateLocalStorage(data){
    localStorage.setItem(TRIP_DATA_KEY, JSON.stringify(data));
}

//Function to get current data from local storage
function getLocalStorage(){
    let retrieve = JSON.parse(localStorage.getItem(TRIP_DATA_KEY));
    return retrieve;
}

//Function to get the item for the past events
function pastPage(){
    localStorage.setItem(TRIP_EVENTS_KEY, 'past')
}

//Function to get the item for the future events
function futurePage(){
    localStorage.setItem(TRIP_EVENTS_KEY, 'future')
}

//Code that will run on load
let check = checkIfDataLocalStorageExist();


if(check === true){ //re-create the tripList class if it is already existed
    let data = getLocalStorage();
    trip.fromData(data);
}
else{ //create a new object of tripList and save it inside local storage if it doesn't exist
    updateLocalStorage(trip);
}


