/*define a function display table with parameter data
function should take all attributes of data using the trips class getters
create an html string and display the string using innerHTML to the div table id
*/
function displayTable(data){
	let airportsList = data.airports;
	let distance = data.distance;
	let totalDist = 0;
	let tableStr =  "<table class=\"mdl-data-table mdl-js-data-table mdl-data-table\" style=\"width:75%\"><thead><th class=\"mdl-data-table__cell--non-numeric\">Trip</th><th>Distance(km)</th></thead><tbody align-content: center>";
	for(let i=0;i<(airportsList.length-1);i++){
		tableStr += "<tr><td class=\"mdl-data-table__cell--non-numeric\">"+airportsList[i].label+" to "+airportsList[i+1].label+"</td><td>"+distance[i]+"</td></tr>";
		totalDist = totalDist + Number(distance[i]);
	}
	tableStr += "<tr><td class=\"mdl-data-table__cell--non-numeric\">Total Distance</td><td>"+totalDist.toFixed(2)+"</td></tr>";
	tableStr += "</tbody></table>";
	document.getElementById("table").innerHTML = tableStr;
}

/*
define function displayInfo with parameter data
using the document.getELementByIS change every available info div IDs and display them to the HTML reference, the displayed information should be obtained using the class Trip getters
*/
function displayInfo(data){
	document.getElementById("tripDate").innerHTML = data.date;
	document.getElementById("stops").innerHTML = data.stops;
	document.getElementById("tripName").innerHTML = data.name;
	document.getElementById("creationDate").innerHTML = data.creationDate;	
}
							
/*
define function showMarkers with parameter data
function should iterate all the airports list and create a marker for each elememt, obtain the longitude, latitude and the airports with the class Trip getters
*/
function showMarkers(data){			
		let airportList = data.airports;
		//show markers
		map.on('load', function(){
			for (let i=0;i<airportList.length;i++){
			var marker = new mapboxgl.Marker({draggable: false})
						.setPopup(new mapboxgl.Popup().setHTML(airportList[i].label))
						.setLngLat(airportList[i].longlat)
						.addTo(map);	 
			}
		})
}

/* 
define function showLine with parameter data
function should define another variable to store the coordinates and iterates all element in the data attribute ._airports and store each longitude latitude in the coordinate variable.
on map load create the line for each coordinates using the variable coordinates
*/
function showLine(data){
	 	//show lines
		let airportList = data.airports;
	 	let coordinates=[];
      	for(i=0;i<airportList.length;i++){
		  coordinates.push(airportList[i].longlat)
	  	}
	map.on('load', function() {
		map.addSource('route', {
			'type': 'geojson',
			'data': {
				'type': 'Feature',
				'properties': {},
				'geometry': {
					'type': 'LineString',
					'coordinates': coordinates
				}
			}
		});
	
		map.addLayer({
			'id': 'route',
			'type': 'line',
			'source': 'route',
			'layout': {
				'line-join': 'round',
				'line-cap': 'round'
			}
		});	
	});
}	


let theIndex = JSON.parse(localStorage.getItem(TRIP_INDEX_KEY));
let eventTime = localStorage.getItem(TRIP_EVENTS_KEY);

trip.fromData(getLocalStorage())

let displayedTrip = new Trip();
displayedTrip.fromData(trip.getTrip(theIndex,eventTime))

mapboxgl.accessToken = 'pk.eyJ1IjoiZGF2aWR5cyIsImEiOiJja250cHdlaHkwNG1mMnFycWV3a2M1dDk4In0.JMug4QX2L0BD0vZ2G1PS4w';
      
//global variable map
var  map = new mapboxgl.Map({
        container: 'map', // container id
        style: 'mapbox://styles/mapbox/streets-v11', // style URL

        center: [displayedTrip.countryLongLat[0],displayedTrip.countryLongLat[1]], // starting position [lng, lat]
        zoom: 3.1 // starting zoom
        });

//function to run on page load
displayTable(displayedTrip)
displayInfo(displayedTrip)
showMarkers(displayedTrip);
showLine(displayedTrip);

