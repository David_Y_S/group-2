/* Write your function for implementing the Jump Search algorithm here */

function jumpSearchTest(data){
    let index = 0
    let point = 0
    let blockSize = 4
    while (point < blockSize){
        while (index <= data.length-1){
            if(data[index].emergency === true ){
                return data[index].address          
            }
            else{
                index += blockSize
            }
        }

        point += 1
        index = point 
    }
    
    if (point >= blockSize){
        return null
    }
    
}