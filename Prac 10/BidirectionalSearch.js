/* Write your function for implementing the Bi-directional Search algorithm here */
//TASK C
function bidirectionalSearchTest(data){
    
    let counter = 0;
    let boundary = (data.length);
    boundary = Math.floor(boundary);
    let middle = boundary/2;
    middle = Math.floor(middle);
    
    while (counter <= (boundary/2)){
        
        if(data[middle-counter].emergency === true){
            return data[middle-counter].address;
        }
        else{
            if(data[(middle+counter)].emergency === true){
                return data[(middle+counter)].address;
            }
            else{
                counter++;
            }
        }
    }
    return null;
}

//coba jo