/*
events.js
This file contains code that runs on load for events.html
*/

//To get the key from local storage of what page is accessed by the user
let eventPage = localStorage.getItem(TRIP_EVENTS_KEY)

let eventArea = document.getElementById('eventSection')
let eventName = "";

//Function to display cards
function displayCards(data){
    let listHTML = "";

if(eventPage === 'past'){
        for(let i = 0; i < data.pastList.length; i++){  
                
            singleTrip.fromData(data.pastList[i]);
			
			let singleTotalDistance = 0;
							
			for(j=0;j<singleTrip.distance.length;j++){
				singleTotalDistance += Number(singleTrip.distance[j]); 
			}
            
            listHTML += "<div class=\"mdl-cell mdl-cell--4-col\"><div class=\"mdl-card mdl-shadow--2dp\" style=\"background-color: antiquewhite\"><div class=\"mdl-card__media\">"

            listHTML += "<img src=\"https://i.pinimg.com/564x/6f/33/db/6f33db9eeb3968e62a022f001b2035e9.jpg\" width=\"310\" height=\"137\" border=\"0\" style=\"padding: 10px; background-color: antiquewhite\"></div><div class=\"mdl-card__title\"><h4 class=\"mdl-card__title-text\">" + singleTrip.name + "</h4></div><div class=\"mdl-card__supporting-text\">"

            listHTML += "<p style=\"line-height: 0.5em\">Date      :" + singleTrip.date + "<span id=\"tripDate\"></span> </p>"
            listHTML += "<p style=\"line-height: 0.5em\">Origin    :" + singleTrip.airports[0].label + "<span id=\"tripOrigin\"></span> </p>" 
            listHTML += "<p style=\"line-height: 0.5em\">Final     :" + singleTrip.airports[singleTrip.airports.length-1].label + "<span id=\"tripFinal\"></span> </p>" 
            listHTML += "<p style=\"line-height: 0.5em\">Stops     :" + singleTrip.stops + "<span id=\"tripStops\"></span> </p>"
            listHTML += "<p style=\"line-height: 0.5em\">Distance  :" +  singleTotalDistance.toFixed(2) + "<span id=\"tripDistance\"></span> </p>"

            listHTML += "<button class=\"mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect\" style=\"background-color: cornsilk\" onclick=\"view(" + i + ")\"> View Details</button></div></div></div>"
        }
    }
        
        //else if the cards from future events
    else if(eventPage === 'future'){
        
        for(let i = 0; i < data.futureList.length; i++){  
            
            singleTrip.fromData(data.futureList[i]);
            
			let singleTotalDistance = 0;
							
			for(j=0;j<singleTrip.distance.length;j++){
				singleTotalDistance += Number(singleTrip.distance[j]); 
			}
			
            listHTML += "<div class=\"mdl-cell mdl-cell--4-col\"><div class=\"mdl-card mdl-shadow--2dp\" style=\"background-color: antiquewhite\"><div class=\"mdl-card__media\">"
            
            listHTML += "<img src=\"https://i.pinimg.com/474x/1e/f2/5c/1ef25c8f1ef8133d5609e3a7ff752172.jpg\" width=\"310\" height=\"137\" border=\"0\" style=\"padding: 10px; background-color: antiquewhite\"></div><div class=\"mdl-card__title\"><h4 class=\"mdl-card__title-text\">" + singleTrip.name + "</h4></div><div class=\"mdl-card__supporting-text\">"
            
            listHTML += "<p style=\"line-height: 0.5em\">Date      :&nbsp;" + singleTrip.date + "<span id=\"tripDate\"></span> </p>"
            listHTML += "<p style=\"line-height: 0.5em\">Origin    :&nbsp;" + singleTrip.airports[0].label + "<span id=\"tripOrigin\"></span> </p>" 
            listHTML += "<p style=\"line-height: 0.5em\">Final     :&nbsp;" + singleTrip.airports[singleTrip.airports.length-1].label + "<span id=\"tripFinal\"></span> </p>" 
            listHTML += "<p style=\"line-height: 0.5em\">Stops     :&nbsp;" + singleTrip.stops + "<span id=\"tripStops\"></span> </p>"
            listHTML += "<p style=\"line-height: 0.5em\">Distance  :&nbsp;" + singleTotalDistance.toFixed(2) + " km<span id=\"tripDistance\"></span> </p>"

            listHTML += "<button class=\"mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect\" style=\"background-color: cornsilk\" onclick=\"view(" + i + ")\"> View Details</button>"

            listHTML += "<button class=\"mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect\" style=\"background-color: mediumaquamarine\" onclick=\"deleteCard("+ i +")\">Delete</button></div></div></div>"
        }
    }

    let ref = document.getElementById('cardDisplay');
    ref.innerHTML = listHTML
}

//Function to see the detailed information of the trip
function view(index){
   		let data = JSON.stringify(index);
		localStorage.setItem(TRIP_INDEX_KEY, data);
    	window.location = "info.html";
}


//Function to delete selected card
function deleteCard(index){
    
    let confirms = confirm("Press OK to delete the card.")
    
    if(confirms === true){
        
        trip.removeTrip(index);
        updateLocalStorage(trip);
        
        alert("The card has been deleted");
        window.location = "events.html"
    }
}

//Funcrion to update the past list
function updatePastTrip(data){

    let today = new Date();
    
	for(let i=0; i < data.futureList.length; i++){
       
        singleTrip.fromData(data.futureList[i]);
        
  		let tripDate = singleTrip.date;
        
        let currYear = today.getFullYear()
        currYear = currYear.toString()
        
        let currMonth = today.getMonth()
        currMonth = currMonth.toString()
        
        let currDay = today.getDate()
        currDay = currDay.toString()
        
       
        
        let tripYear = tripDate.substring(0,4)
        let tripMonth = tripDate.substring(5,7)
        let tripDay = tripDate.substring(8,10)
        
        
        
        if((tripYear <= currYear) && (tripMonth <= currMonth ) && (tripDay < currDay)){
            data.addPastTrip(data.futureList[i])
            data.futureList.splice(i,1)
        }
    }
}

//Function that will run on load
if(eventPage === 'past'){
    eventName += "<b>&nbsp; Past</b>"
    eventArea.innerHTML = eventName
    
}
else if(eventPage === 'future'){
    eventName += "<b>&nbsp; Future</b>"
    eventArea.innerHTML = eventName
}

//To display the cards of selected events page
let currentTrip = getLocalStorage();
trip.fromData(currentTrip);
updatePastTrip(trip);
displayCards(trip);
